# FastLBRY-GTK setup (without needing LBRY Desktop)

Useful for platforms where Electron based LBRY Desktop client cannot be built and used. For example, on FreeBSD.


## How to login

Signing in without a GUI client has been an issue which has mostly [remained](https://forum.lbry.com/t/how-to-sign-in-and-upload-video-using-the-lbry-sdk/312) [unsolved](https://forum.kodi.tv/showthread.php?tid=360585). But thanks to the `sign-lbrynet.py` script it is now possible (unless Odysee.com decides to do something to stop it).

TODO: Add instructions for python requests install? Move python3 setup instructions before this?

- Run the lbrynet daemon (`lbrynet start`, then wait for it to initialize)
- Run the [`signin-lbrynet.py`](../scripts/signin-lbrynet.py) script
- Follow the on-screen instructions
- When completed, run the script again and when it shows the auth token, press Ctrl+C. This is your auth token for this machine which you'll need later to get some additional features. Keep the auth token somewhere in your notes.
- When done, I like to stop the lbrynet daemon (Ctrl+C on running term or `lbrynet stop`) because FastLBRY can start the daemon automatically for me later.


## How to setup FastLBRY-GTK

FastLBRY-GTK is a GUI alternative to the Electron powered, bloated LBRY Desktop client. FastLBRY-GTK is comparatively lighter and doesn't require Electron. I had much comfortable experience with it, even on older, low-powered hardware.

1. Have `lbrynet` binary for your platform at hand. You can build it yourself. For example, here are guides for: [Devuan (32-bit)](lbry-on-devuan-32bit.md), [FreeBSD](lbry-on-freebsd.md), [NetBSD](lbry-on-netbsd.md)
2. Place the binary like:

```sh
$ mkdir ~/Repos  # or wherever; but change in the following instructions accordingly
$ cd ~/Repos
$ git clone --depth 1 https://notabug.org/jyamihud/FastLBRY-GTK.git
$ cd FastLBRY-GTK
#~ FastLBRY can start when lbrynet binary is kept in "flbry/lbrynet"
#~ So copy lbrynet binary into flbry directory
$ cp flbry/lbrynet flbry/lbrynet_backup
#~ If you have it downloaded:
$ cp ~/Downloads/lbrynet flbry/lbrynet
$ chmod +x flbry/lbrynet
#~ or if you have it installed in /usr/local/bin
$ ln -s /usr/local/bin/lbrynet ~/Repos/FastLBRY-GTK/flbry
```

3. Install reuired Python modules. You will have to have to make sure you can run `python3` to easily launch FastLBRY GTK:

```sh
#~ FreeBSD:
$ doas pkg install python3

#~ NetBSD
#~ If it's not installed, install latest version of Python 3 available
$ PYLATEST=$(pkgin se '^python3' | sort -t. -n -k1,1 -k2,2 -k3,3 -k4,4 | sed -ne 's/\(^py.*[0-9]\)\-.*/\1/gp' | tail -n1)
$ find /usr/pkg/bin -name 'python3\.[0-9][2]' -type f &>/dev/null || doas pkgin install $PYLATEST
#~ To be able to just execute "python3". Useful later for running commands
#~ and scripts.
$ PYFOUND=$(find /usr/pkg/bin -name 'python3\.[0-9][2]' -type f | tail -n1)
$ [ $PYFOUND ] && doas ln -s $PYFOUND /usr/pkg/bin/python3
```

Now test if `python3` is available. Example:

```sh
$ python3 --version
Python 3.12.6
```

This will also make finding the default python version easy for installing python module packages:

```sh
$ PYV="py$(python3 -c 'import sys; print(str(sys.version_info.major)+str(sys.version_info.minor))')"
$ echo $PYV
py312
```

You might have to install other packages like:

```sh
#~ FreeBSD:
$ doas pkg install $PYV-gobject3 $PYV-pillow gtk3
#~ NetBSD:
$ doas pkgin install $PYV-gobject3 $PYV-Pillow gtk3+
```


4. To make it easy for us to run FastLBRY-GTK, create a file named `run.sh` and put this in it:

```sh
#!/usr/bin/env bash
cd "$(dirname "$0")"
python3 run.py
```

Make the file executable:

```sh
$ chmod +x run.sh
```

If your file manager is configured to be able to run scripts with double click on the script files, you can now double click on `run.sh` to run FastLBRY-GTK. But we want to be more lazy...

5. Make FastLBRY-GTK available in the standard application menu:

Create a file `~/.local/share/applications/fastlbry-gtk.desktop`. Put the content below in that file:

```
[Desktop Entry]
Type=Application
Name=FastLBRY GTK
GenericName=LBRY Browser
Comment=Enjoy content over LBRY protocol
Icon=lbry
Exec=/home/usernamehere/Repos/FastLBRY-GTK/run.sh
Terminal=false
Categories=AudioVideo;Audio;Video;Player;TV;
```

Then run this as your normal user to place your name in automatically:

```sh
$ sed -i -e "/^Exec/ s/usernamehere/$USER/" ~/.local/share/applications/fastlbry-gtk.desktop
```

Now you should see a new item named "FastLBRY GTK" in your application menu or launcher or where you find your standard applications in your desktop session.


## Running and configuring

Now run "FastLBRY GTK". Click the "Connect" button. This will start lbrynet daemon for you. From user menu (shows by clicking the button with a person icon) -> Settings you can configure it to start lbrynet daemon automatically when FastLBRY GTK starts.

If you want to see latest comments on the user menu (shows by clicking the button with a person icon) you will have to input the auth token in the user menu (person icon) -> Settings -> Account / Wallet -> Authentication Token field. While in Settings, feel free to look around if you want to change any settings. Although it is designed for intermediate users and misconfiguring something might make something unusuable.

If you are interested in the details, the settings are stored in `~/.local/share/flbry/` dir. If you mess up anything in settings, close FastLBRY GTK, delete this directory, lauch FastLBRY GTK and start over.


## How to watch content

By default it opens the "Following" page. There are also other options. Click the user menu (person icon) and choose anything from the second column, second row. You can choose, for example, say "Trending" or "Suggested".

To watch a video:

- Middle click on a video thumbnail to open on a new tab (because there is no back button implemented yet)
- Click Download
- When download finishes, click Launch
