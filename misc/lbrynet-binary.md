# lbrynet binary downloads

## Linux (x86_64), Windows, Mac

<https://github.com/lbryio/lbry-sdk/releases>

## Linux (i386)

(no binary available, requires building from source)

## FreeBSD (x86_64)

<https://mia.nl.tab.digital/s/35QXsSRtdAdHQj3>

## NetBSD (x86_64)

<https://mia.nl.tab.digital/s/qBXW89QcLPmDqWc>
